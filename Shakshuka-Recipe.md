**Shakshuka Recipe** 

Ingredients: 
- extra virgin olive oil 
- 1 large chopped onion, 
- 1 to 2 green bell peppers
- 2 minced garlic cloves
- Spices: 1 ts coriander, 1 ts cumin, 1 ts paprika, black papper, solt
- 1 can of chopped tomatoes
- 4 eggs
- fresh parsley or coriander

How to: 
1.  Start by sauteing chopped onions, bell peppers, and garlic with a little bit of extra virgin olive oil in a large, heavy skillet or pan 
2. Season with kosher salt and coriander, paprika, cumin, and crushed pepper flakes. Cook for at least 5 minutes, stirring, until the veggies are tender, then add fresh diced tomatoes 
3. Bring the tomatoes to a boil, then cover and let simmer for about 15 minutes, then uncover and cook a few more minutes until your sauce has thickened. 
4. When the sauce is ready, use the back of a spoon and make some holes in the sauce. Crack your eggs and nestle each egg in one of the holes you created. Cover the skillet and allow the eggs to simmer in the sauce over medium-low heat until the egg whites have settled (as I said earlier, the eggs are supposed to be soft and somewhat runny.
5.  Once the eggs are ready, garnish with parsley and serve immediately!