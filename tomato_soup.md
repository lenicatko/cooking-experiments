Tomato soup

Ingredients: 
1 tbs of olive oil
bit of butter
1 tbs of sugar
1 can of chopped tomatoes
1 bottle of passata
1 cup of water
Seasoning: One bay leaf, nové korenie, salt, papper, chilli

How to: 
1. Caramelize the sugar with olive oil and butter
2. Add tomatoes and tomato passata, cup of water and seasoning
3. Cook for 20-25 minutes (at home, the cooker set up on soup and 4 minutes)
4. Add basil leaf for decoration and can be served with baguette. 