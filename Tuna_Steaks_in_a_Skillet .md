**Tuna Steaks in a Skillet** 

Ingredients: 
- tuna steak
- olive oil
- lemon
- Seasoning: salt, black pepper, rosemary

How to: 
1. Before cooking tuna steaks, rinse with cool water. Pat dry with paper towels. 
2. Measure the thickness of the fish, so you know how long to cook tuna steak. 
3. Marinade: A short marinating time is all it needs, from 15 minutes to 4 hours in the refrigerator. Brush the fish with olive oil or melted butter and lemon jouse, then sprinkle it with snipped fresh herbs (such as rosemary or tarragon), salt, and ground black pepper.
4. Add 1 Tbsp. oil and 1 Tbsp. butter to the skillet. Heat the skillet over medium-high heat until hot.
Add the tuna steaks. The steaks should sizzle when added. Cook, uncovered, 4 to 6 minutes per 1,3 cm thickness (6 to 9 minutes for the 1,9cm-thick steaks we suggest) or until fish begins to flake when tested with a fork but is still pink in the center, turning once during cooking. Adjust the heat as needed if the skillet gets too hot.
